<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <?php wp_head();?>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body class="container">

<nav>
    <a href="<?php echo get_home_url();?>">
        <img src="<?php echo get_stylesheet_directory_uri();?>/images/logos/inhabitent-logo-tent-white.svg" alt="Inhabitent Tent Logo" width="80px" height="auto"/>
    </a>
    <div class="nav-text nav-right">
        <?php wp_nav_menu(array('theme_location' => 'primary'));?>
    </div>
</nav>