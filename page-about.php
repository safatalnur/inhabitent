<?php get_header(); ?>

<?php if( have_posts() ) :

//The WordPress Loop: loads post content 
    while( have_posts() ) :
        the_post(); ?>
    
    <!-- <?php the_content(); ?> -->
    <div class="about-page-container">
        <img src="<?php echo get_home_url();?>/wp-content/uploads/2021/11/About_hero_resized.jpg" alt="About Page Hero Image" width="100%" height="auto"/>
        <h2><?php the_title();?></h2>
    </div>

    <div class="about-content-container">
        <h2>Our Story</h2>
        <p>Inhabitent Camping Supply Co. has been Vancouver baked-good icon for more than two whole months! Customers often comment on the bustle of activity they see in store…that’s where the magic happens every day.

        We want to bridge the gap between the comfort of city life and the lovely Instagram-worthiness of the great outdoors that surround us. We sell gear that’s fun and functional. So much fun, in fact, that you’ll want to pitch a tent inside your one-bedroom apartment so you can use it everyday.</p>
    </div>

    <div class="about-content-container">
        <h2>Our Team</h2>
        <p>
            Inhabitent Camping Supply Co.’s staff is made up of an amazing team of inspired retail associates. We really know our stuff when it comes to travel hammocks and campfire cooking gadgets. From a provincial park campground to the back-country, our staff knows what you need to outfit your outdoor outing.
            Our shop in nestled away in a lovely little corner of Vancouver. Pop in, say hi, and try out our tents!
        </p>
    </div>
    <div class="page-spacer">

    </div>

    
    <!-- Loop ends -->
    <?php endwhile;?>

    <?php the_posts_navigation();?>

<?php else : ?>
        <p>No posts found</p>
<?php endif;?>

    
<?php get_footer();?>