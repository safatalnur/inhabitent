<?php get_header(); ?>


<?php if( have_posts() ) :
//The WordPress Loop: loads post content 
    while( have_posts() ) :
        the_post(); ?>
    
    <div class="findus-container">
        <div class="findus-content">
            <h2><?php the_title(); ?></h2>
            <!-- <?php the_content(); ?> -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2603.683173177875!2d-123.14036128396947!3d49.26345077932912!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548673c79e1ac457%3A0x3aea6428fa30dc6a!2s1490%20W%20Broadway%2C%20Vancouver%2C%20BC%20V6H%204E8!5e0!3m2!1sen!2sca!4v1637816285155!5m2!1sen!2sca" width="95%" height="350" style="border:10px solid #e1e1e1;" allowfullscreen="" loading="lazy"></iframe>

            <h2>We take camping very seriously</h2>

            <p>Inhabitent Camping Supply Co. knows what it takes to outfit a camping trip right. From flannel shirts to artisanal axes, we've got your covered. Please contact us below with any questions comments or suggestions.</p>

            <h2>Send Us Email!</h2>

            <form action="mailto:safatalnur@gmail.com" method="post" enctype="text/plain">
                <p>NAME:</p>
                <input type="text" name="name">
                <p>EMAIL:</p>
                <input type="email" name="mail">
                <p>Subject:</p>
                <input type="text" name="subject">
                <p>Comment:</p>
                <input type="text" name="comment" width="100px">
                <input type="submit">
            </form> 

            <?php endwhile;?>

            <!-- Loop ends -->


            <?php else : ?>
                    <p>No posts found</p>
            <?php endif;?>

        </div>

        <div>
            <?php get_sidebar();?>
        </div>
    
    </div>

    
<?php get_footer();?>