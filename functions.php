<?php

//Adds script and stylesheets
function inhabitent_files() {
    wp_enqueue_style('inhabitent_styles', get_stylesheet_uri('/build/css/style.min.css'), NULL, microtime());
    wp_enqueue_style('fonts', "https://fonts.googleapis.com/css?family=Lato&display=swap");
    wp_enqueue_style('font-awesome', "https://use.fontawesome.com/releases/v5.15.4/css/all.css");
}

add_action('wp_enqueue_scripts', 'inhabitent_files');

//Adds theme support - ex: title tag
function inhabitent_features() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    register_nav_menus(array(
        'primary' => 'Primary Navigation Menu',
        'secondary' => 'Secondary Menu'
    ));
}

add_action('after_setup_theme', 'inhabitent_features');

function inhabitent_widgets() {
    register_sidebar(array(
        'name'          => "Sidebar 1",
        'id'            => "sidebar-1",
        'description'   => "Contact Info Widget",
        'class'         => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>\n',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>\n',
    ));
}

add_action('widgets_init', 'inhabitent_widgets');

?>