    <footer>
        <div class="footer-container">
            <div class="footer-content footer-info">
                <h4>Contact Us</h4>
                <p><i class="fas fa-envelope"><span>info@inhabitent.com</span></i></p>
                <p><i class="fas fa-phone-alt"><span>778-456-7891</span></i></p>
                <p>
                    <i class="fab fa-facebook-square"></i>
                    <span><i class="fab fa-twitter-square"></i></span>
                    <span><i class="fab fa-google-plus-square"></i></span>
                </p>
            </div>
            
            <div class="footer-content footer-info">
                <h4>Business Hours</h4>
                <p>
                    Monday - Friday:<span>9am to 5pm</span><br>
                    Saturday:<span>10am to 2pm</span><br>
                    Sunday:<span>Closed</span>
                </p>
            </div>
            
            <div class="footer-content footer-image">
                <img src="<?php echo get_template_directory_uri() . '/images/logos/inhabitent-logo-text.svg';?>"/>
            </div>   
        </div>

        <div class="footer-copyright">
            <p>COPYRIGHT &copy; <?php echo date("Y");?> Inhabitent</p>
        </div>

    </footer>

    <?php wp_footer();?>

    </body>
</html>